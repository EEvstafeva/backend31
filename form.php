
<!DOCTYPE html> <html lang="ru"> <head>
  <link rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script
			src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
    <title> Задание 1 </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width= device-width, initial-scale=1.0">
  </head>
  <body>
    <form action="."
    method="POST">

    <label>
      Имя:<br />
      <input name="name"
        value="Ваше имя" />
    </label><br />

    <label>
      Email:<br />
      <input name="email"
        value="Ваш email"
        type="email" />
    </label><br />

    <label>
      Дата рождения:<br />
      <input name="birthday"
        value="2021-03-29"
        type="date" />
    </label><br />
    <label>
      Биография:<br />
      <textarea name="biography">...</textarea>
    </label><br />

      Количество ваших конечностей: <br />
      <label>
        <input type="radio" checked="checked"
        name="limbs" value="1" />
        От 0 до 5
      </label>
      <label>
        <input type="radio"
        name="limbs" value="2" />
        От 6 до 10
      </label>
      <label>
        <input type="radio"
          name="limbs" value="3" />
          11 и более
        </label>
        <br />

      Укажите ваши сверхспособности:
      <br />
      <select name="abilitiess[]"
        multiple="multiple">
        <option value="1" selected="selected">Чтение мыслей</option>
        <option value="2">Перерождение</option>
        <option value="3">Управление солнечным светом</option>
        <option value="4">Заговаривание воды</option>
      </select>
    <br />

    Ваш пол:<br />
    <label><input type="radio" checked="checked"
      name="gender" value="M" />
      Муж</label>
    <label><input type="radio"
      name="gender" value="F" />
      Жен</label><br />

    <br />
    <p> <label><input type="checkbox" checked="checked"
      name="contract" />
      С контрактом ознакомлен(а)</label></p>

    <input type="submit" value="Отправить" />
  </form>
  </body>
</html>
