
<?php
header('Content-Type: text/html; charset=UTF-8');

error_reporting(E_ERROR | E_PARSE);

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print "<div id='success_form_response'>
                Ваши результаты сохранены.
            </div>";
  }
    include('form.php');
    exit();
}

$errors = FALSE;

$errors_message = "Данные не отправились из-за следующих ошибок: <br/>";

$trimmedPost = [];

foreach ($_POST as $key => $value)
	if (is_string($value))
		$trimmedPost[$key] = trim($value);
	else
		$trimmedPost[$key] = $value;

if (empty($trimmedPost['name'])) {
  $errors_message.= 'Вы неверно заполнили имя.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimmedPost['email'])) {
  $errors_message.= 'Вы неверно заполнили имеил.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimmedPost['birthday'])) {
  $errors_message.= 'Вы неверно заполнили дату рождения.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[MF]$/', $trimmedPost['gender'])) {
  $errors_message.= 'Вы неверно ввели пол.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[1-3]$/', $trimmedPost['limbs'])) {
  $errors_message.= 'Вы неверно ввели количество конечностей.<br/>';
  $errors = TRUE;
}
$superpowers_error = FALSE;
foreach ($trimmedPost['abilities'] as $v)
  if (!preg_match('/[1-3]/', $v)) {
    $superpowers_error = TRUE;
    $errors = TRUE;
  }
if($superpowers_error)
  $errors_message.= 'Вы неверно ввели Суперспособности.<br/>';
if (!isset($trimmedPost['contract'])) {
  $errors_message.= 'Согласитесь с обработкой персональных данных.<br/>';
  $errors = TRUE;
}

if ($errors) {
  include('form.php');
  print "<link href='show_mb.css' rel='stylesheet'>
            <div id='modal_blackout'>
              <div id='success_form_response'>";
  echo "{$errors_message}";  
  print "</div>
            </div>";           
  exit();
}

$user = 'u41080';
$pass = '2491072';
$db = new PDO('mysql:host=localhost;dbname=u41080', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $db->beginTransaction();
  $stmt1 = $db->prepare("INSERT INTO forma SET name = ?, email = ?, birthday = ?, 
    gender = ? , limbs = ?, biography = ?");
  $stmt1 -> execute([$trimmedPost['name'], $trimmedPost['email'], $trimmedPost['birthday'], 
    $trimmedPost['gender'], $trimmedPost['limbs'], $trimmedPost['biography']]);
  $stmt2 = $db->prepare("INSERT INTO clients_capabilities SET id_client = ?, id_ability = ?");
  $id = $db->lastInsertId();
  foreach ($trimmedPost['abilitiess'] as $s){
    $stmt2 -> execute([$id, $s]);

$stmt3 = $db->prepare("INSERT INTO capabilities SET id_client = ?, name_ability = ?");
$stmt3 -> execute([$s, form.hero.abilitiess[$s].value]);  
  }
  $db->commit();
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  $db->rollBack();
  exit();
}


//foreach ($trimmedPost['abilitiess'] as $s){
//$stmt3 = $db->prepare("INSERT INTO capabilities SET id_client = ?, name_ability = ?");
//$stmt3 -> execute([$s, f.hero.abilitiess[$s].value]);  
//}

//$stmt = $db->prepare("INSERT INTO capabilities (id_client,name_ability) VALUES (:id_client,//:name_abil)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));

header('Location: ?save=1');
